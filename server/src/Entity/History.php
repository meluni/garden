<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\HistoryRepository")
 */
class History
{
    public const ACTION_START = 0;
    public const ACTION_STOP  = 1;

    public const ACTION_LABEL = [
        self::ACTION_START => 'start',
        self::ACTION_STOP  => 'stop',
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Routine", inversedBy="histories")
     */
    private $routine;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\RoutineSchedule", inversedBy="histories")
     */
    private $routineSchedule;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\SprinklerGroup", inversedBy="histories")
     * @ORM\JoinColumn(nullable=false)
     */
    private $sprinklerGroup;

    /**
     * @ORM\Column(type="smallint")
     */
    private $action;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRoutine(): ?Routine
    {
        return $this->routine;
    }

    public function setRoutine(?Routine $routine): self
    {
        $this->routine = $routine;

        return $this;
    }

    public function getRoutineSchedule(): ?RoutineSchedule
    {
        return $this->routineSchedule;
    }

    public function setRoutineSchedule(?RoutineSchedule $RoutineSchedule): self
    {
        $this->routineSchedule = $RoutineSchedule;

        return $this;
    }

    public function getSprinklerGroup(): ?SprinklerGroup
    {
        return $this->sprinklerGroup;
    }

    public function setSprinklerGroup(?SprinklerGroup $sprinklerGroup): self
    {
        $this->sprinklerGroup = $sprinklerGroup;

        return $this;
    }

    public function getAction(): ?int
    {
        return $this->action;
    }

    public function setAction(int $action): self
    {
        $this->action = $action;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function toArray(): array
    {
        return [
            'id'              => $this->getId(),
            'routine'         => $this->getRoutine() ? $this->getRoutine()->toArray() : null,
            'routineSchedule' => $this->getRoutineSchedule() ? $this->getRoutineSchedule()->toArray() : null,
            'sprinklerGroup'  => $this->getSprinklerGroup()->toArray(),
            'action'          => self::ACTION_LABEL[$this->getAction()],
            'createdAt'       => $this->getCreatedAt()->format('Y-m-d H:i:s'),
        ];
    }
}
