<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SprinklerRepository")
 */
class Sprinkler
{
    public const STATUS_DISABLED = 0;
    public const STATUS_ENABLED = 1;

    public const STATUS_LABEL = [
        self::STATUS_DISABLED => 'disabled',
        self::STATUS_ENABLED => 'enabled',
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\SprinklerGroup", inversedBy="sprinklers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $sprinklerGroup;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="smallint")
     */
    private $positionX;

    /**
     * @ORM\Column(type="smallint")
     */
    private $positionY;

    /**
     * @ORM\Column(type="smallint")
     */
    private $status;

    /**
     * @ORM\Column(type="string", length=5000)
     */
    private $area;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSprinklerGroup(): ?SprinklerGroup
    {
        return $this->sprinklerGroup;
    }

    public function setSprinklerGroup(?SprinklerGroup $sprinklerGroup): self
    {
        $this->sprinklerGroup = $sprinklerGroup;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPositionX(): ?int
    {
        return $this->positionX;
    }

    public function setPositionX(int $positionX): self
    {
        $this->positionX = $positionX;

        return $this;
    }

    public function getPositionY(): ?int
    {
        return $this->positionY;
    }

    public function setPositionY(int $positionY): self
    {
        $this->positionY = $positionY;

        return $this;
    }

    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'positionX' => $this->getPositionX(),
            'positionY' => $this->getPositionY(),
            'status' => self::STATUS_LABEL[$this->getStatus()],
            'area' => json_decode($this->getArea()),
            'sprinklerGroup' => [
                'id' => $this->getSprinklerGroup()->getId(),
            ]
        ];
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getArea(): ?string
    {
        return $this->area;
    }

    public function setArea(string $area): self
    {
        $this->area = $area;

        return $this;
    }
}
