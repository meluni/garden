<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SprinklerGroupRepository")
 */
class SprinklerGroup
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Sprinkler", mappedBy="sprinklerGroup", orphanRemoval=true)
     */
    private $sprinklers;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\RoutineAction", mappedBy="sprinklerGroup", orphanRemoval=true)
     */
    private $routineActions;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\History", mappedBy="sprinklerGroup")
     */
    private $histories;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SprinklerGroupQueue", mappedBy="sprinklerGroup")
     */
    private $sprinklerGroupQueues;

    /**
     * @ORM\Column(type="smallint")
     */
    private $pin;

    public function __construct()
    {
        $this->sprinklers = new ArrayCollection();
        $this->routineActions = new ArrayCollection();
        $this->histories = new ArrayCollection();
        $this->sprinklerGroupQueues = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Sprinkler[]
     */
    public function getSprinklers(): Collection
    {
        return $this->sprinklers;
    }

    public function addSprinkler(Sprinkler $sprinkler): self
    {
        if (!$this->sprinklers->contains($sprinkler)) {
            $this->sprinklers[] = $sprinkler;
            $sprinkler->setSprinklerGroup($this);
        }

        return $this;
    }

    public function removeSprinkler(Sprinkler $sprinkler): self
    {
        if ($this->sprinklers->contains($sprinkler)) {
            $this->sprinklers->removeElement($sprinkler);
            // set the owning side to null (unless already changed)
            if ($sprinkler->getSprinklerGroup() === $this) {
                $sprinkler->setSprinklerGroup(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|RoutineAction[]
     */
    public function getRoutineActions(): Collection
    {
        return $this->routineActions;
    }

    public function addRoutineAction(RoutineAction $routineAction): self
    {
        if (!$this->routineActions->contains($routineAction)) {
            $this->routineActions[] = $routineAction;
            $routineAction->setSprinklerGroup($this);
        }

        return $this;
    }

    public function removeRoutineAction(RoutineAction $routineAction): self
    {
        if ($this->routineActions->contains($routineAction)) {
            $this->routineActions->removeElement($routineAction);
            // set the owning side to null (unless already changed)
            if ($routineAction->getSprinklerGroup() === $this) {
                $routineAction->setSprinklerGroup(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|History[]
     */
    public function getHistories(): Collection
    {
        return $this->histories;
    }

    public function addHistory(History $history): self
    {
        if (!$this->histories->contains($history)) {
            $this->histories[] = $history;
            $history->setSprinklerGroup($this);
        }

        return $this;
    }

    public function removeHistory(History $history): self
    {
        if ($this->histories->contains($history)) {
            $this->histories->removeElement($history);
            // set the owning side to null (unless already changed)
            if ($history->getSprinklerGroup() === $this) {
                $history->setSprinklerGroup(null);
            }
        }

        return $this;
    }

    public function toArray(): array
    {
        $status = Sprinkler::STATUS_DISABLED;
        foreach ($this->getSprinklers() as $sprinkler) {
            if ($sprinkler->getStatus() === Sprinkler::STATUS_ENABLED) {
                $status = Sprinkler::STATUS_ENABLED;
                break;
            }
        }

        $data = [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'status' => Sprinkler::STATUS_LABEL[$status],
            'pin' => $this->getPin(),
            'sprinklers' => [],
        ];

        foreach ($this->getSprinklers() as $sprinkler) {
            $data['sprinklers'][] = $sprinkler->toArray();
        }

        return $data;
    }

    /**
     * @return Collection|SprinklerGroupQueue[]
     */
    public function getSprinklerGroupQueues(): Collection
    {
        return $this->sprinklerGroupQueues;
    }

    public function addSprinklerGroupQueue(SprinklerGroupQueue $sprinklerGroupQueue): self
    {
        if (!$this->sprinklerGroupQueues->contains($sprinklerGroupQueue)) {
            $this->sprinklerGroupQueues[] = $sprinklerGroupQueue;
            $sprinklerGroupQueue->setSprinklerGroup($this);
        }

        return $this;
    }

    public function removeSprinklerGroupQueue(SprinklerGroupQueue $sprinklerGroupQueue): self
    {
        if ($this->sprinklerGroupQueues->contains($sprinklerGroupQueue)) {
            $this->sprinklerGroupQueues->removeElement($sprinklerGroupQueue);
            // set the owning side to null (unless already changed)
            if ($sprinklerGroupQueue->getSprinklerGroup() === $this) {
                $sprinklerGroupQueue->setSprinklerGroup(null);
            }
        }

        return $this;
    }

    public function getPin(): ?int
    {
        return $this->pin;
    }

    public function setPin(int $pin): self
    {
        $this->pin = $pin;

        return $this;
    }
}
