<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SprinklerGroupQueueRepository")
 */
class SprinklerGroupQueue
{
    public const STATUS_DISABLED = 0;
    public const STATUS_ENABLED = 1;

    public const STATUS_LABEL = [
        self::STATUS_DISABLED => 'disabled',
        self::STATUS_ENABLED => 'enabled',
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\SprinklerGroup", inversedBy="sprinklerGroupQueues")
     * @ORM\JoinColumn(nullable=false)
     */
    private $sprinklerGroup;

    /**
     * @ORM\Column(type="datetime")
     */
    private $startAt;

    /**
     * @ORM\Column(type="smallint")
     */
    private $action;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSprinklerGroup(): ?SprinklerGroup
    {
        return $this->sprinklerGroup;
    }

    public function setSprinklerGroup(?SprinklerGroup $sprinklerGroup): self
    {
        $this->sprinklerGroup = $sprinklerGroup;

        return $this;
    }

    public function getStartAt(): ?\DateTimeInterface
    {
        return $this->startAt;
    }

    public function setStartAt(\DateTimeInterface $startAt): self
    {
        $this->startAt = $startAt;

        return $this;
    }

    public function getAction(): ?int
    {
        return $this->action;
    }

    public function setAction(int $action): self
    {
        $this->action = $action;

        return $this;
    }
}
