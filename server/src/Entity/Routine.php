<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RoutineRepository")
 */
class Routine
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\RoutineAction", mappedBy="routine")
     */
    private $routineActions;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\RoutineSchedule", mappedBy="routine", orphanRemoval=true)
     */
    private $routineSchedules;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\History", mappedBy="routine")
     */
    private $histories;

    public function __construct()
    {
        $this->routineActions = new ArrayCollection();
        $this->routineSchedules = new ArrayCollection();
        $this->histories = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|RoutineAction[]
     */
    public function getRoutineActions(): Collection
    {
        return $this->routineActions;
    }

    /**
     * @return array|RoutineAction[]
     */
    public function getSortedRoutineActions(): array
    {
        $actions = $this->getRoutineActions();
        $sorted = [];
        foreach ($actions as $action) {
            $sorted[$action->getPosition()] = $action;
        }

        ksort($sorted);

        return $sorted;
    }

    public function addRoutineAction(RoutineAction $routineAction): self
    {
        if (!$this->routineActions->contains($routineAction)) {
            $this->routineActions[] = $routineAction;
            $routineAction->setRoutine($this);
        }

        return $this;
    }

    public function removeRoutineAction(RoutineAction $routineAction): self
    {
        if ($this->routineActions->contains($routineAction)) {
            $this->routineActions->removeElement($routineAction);
            // set the owning side to null (unless already changed)
            if ($routineAction->getRoutine() === $this) {
                $routineAction->setRoutine(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|RoutineSchedule[]
     */
    public function getRoutineSchedules(): Collection
    {
        return $this->routineSchedules;
    }

    public function addRoutineSchedule(RoutineSchedule $routineSchedule): self
    {
        if (!$this->routineSchedules->contains($routineSchedule)) {
            $this->routineSchedules[] = $routineSchedule;
            $routineSchedule->setRoutine($this);
        }

        return $this;
    }

    public function removeRoutineSchedule(RoutineSchedule $routineSchedule): self
    {
        if ($this->routineSchedules->contains($routineSchedule)) {
            $this->routineSchedules->removeElement($routineSchedule);
            // set the owning side to null (unless already changed)
            if ($routineSchedule->getRoutine() === $this) {
                $routineSchedule->setRoutine(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|History[]
     */
    public function getHistories(): Collection
    {
        return $this->histories;
    }

    public function addHistory(History $history): self
    {
        if (!$this->histories->contains($history)) {
            $this->histories[] = $history;
            $history->setRoutine($this);
        }

        return $this;
    }

    public function removeHistory(History $history): self
    {
        if ($this->histories->contains($history)) {
            $this->histories->removeElement($history);
            // set the owning side to null (unless already changed)
            if ($history->getRoutine() === $this) {
                $history->setRoutine(null);
            }
        }

        return $this;
    }

    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
        ];
    }
}
