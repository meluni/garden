<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RoutineScheduleRepository")
 */
class RoutineSchedule
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Routine", inversedBy="routineSchedules")
     * @ORM\JoinColumn(nullable=false)
     */
    private $routine;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $day;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $minute;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $hour;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $weekDay;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $month;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\History", mappedBy="RoutineSchedule")
     */
    private $histories;

    public function __construct()
    {
        $this->histories = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRoutine(): ?Routine
    {
        return $this->routine;
    }

    public function setRoutine(?Routine $routine): self
    {
        $this->routine = $routine;

        return $this;
    }

    public function getDay(): ?string
    {
        return $this->day;
    }

    public function setDay(string $day): self
    {
        $this->day = $day;

        return $this;
    }

    public function getMinute(): ?string
    {
        return $this->minute;
    }

    public function setMinute(string $minute): self
    {
        $this->minute = $minute;

        return $this;
    }

    public function getHour(): ?string
    {
        return $this->hour;
    }

    public function setHour(string $hour): self
    {
        $this->hour = $hour;

        return $this;
    }

    public function getWeekDay(): ?string
    {
        return $this->weekDay;
    }

    public function setWeekDay(string $weekDay): self
    {
        $this->weekDay = $weekDay;

        return $this;
    }

    public function getMonth(): ?string
    {
        return $this->month;
    }

    public function setMonth(string $month): self
    {
        $this->month = $month;

        return $this;
    }

    /**
     * @return Collection|History[]
     */
    public function getHistories(): Collection
    {
        return $this->histories;
    }

    public function addHistory(History $history): self
    {
        if (!$this->histories->contains($history)) {
            $this->histories[] = $history;
            $history->setRoutineSchedule($this);
        }

        return $this;
    }

    public function removeHistory(History $history): self
    {
        if ($this->histories->contains($history)) {
            $this->histories->removeElement($history);
            // set the owning side to null (unless already changed)
            if ($history->getRoutineSchedule() === $this) {
                $history->setRoutineSchedule(null);
            }
        }

        return $this;
    }

    public function toArray(): array
    {
        return [
            'id'      => $this->getId(),
            'routine' => $this->getRoutine()->toArray(),
            'minute'  => $this->getMinute(),
            'hour'    => $this->getHour(),
            'day'     => $this->getDay(),
            'month'   => $this->getMonth(),
            'weekDay' => $this->getWeekDay(),
        ];
    }

}
