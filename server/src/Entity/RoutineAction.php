<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RoutineActionRepository")
 */
class RoutineAction
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Routine", inversedBy="routineActions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $routine;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\SprinklerGroup", inversedBy="routineActions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $sprinklerGroup;

    /**
     * @ORM\Column(type="smallint")
     */
    private $duration;

    /**
     * @ORM\Column(type="smallint")
     */
    private $position;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRoutine(): ?Routine
    {
        return $this->routine;
    }

    public function setRoutine(?Routine $routine): self
    {
        $this->routine = $routine;

        return $this;
    }

    public function getSprinklerGroup(): ?SprinklerGroup
    {
        return $this->sprinklerGroup;
    }

    public function setSprinklerGroup(?SprinklerGroup $sprinklerGroup): self
    {
        $this->sprinklerGroup = $sprinklerGroup;

        return $this;
    }

    public function getDuration(): ?int
    {
        return $this->duration;
    }

    public function setDuration(int $duration): self
    {
        $this->duration = $duration;

        return $this;
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }

    public function setPosition(int $position): self
    {
        $this->position = $position;

        return $this;
    }

    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'routine' => $this->getRoutine()->toArray(),
            'sprinklerGroup' => $this->getSprinklerGroup()->toArray(),
            'duration' => $this->getDuration(),
            'position' => $this->getPosition(),
        ];
    }
}
