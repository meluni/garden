<?php

namespace App\Service;

use App\Command\QueueRunCommand;
use App\Command\QueueScheduleCommand;
use App\Entity\Routine;
use App\Entity\SprinklerGroupQueue;
use GO\Scheduler;

/**
 * Class RoutineService
 *
 * @package App\Service
 */
class RoutineService extends AbstractService
{
    /**
     * @param Routine $routine
     *
     * @throws \Exception
     */
    public function activate(Routine $routine): void
    {
        $scheduler = new Scheduler();
        $scheduler->php(__DIR__ . '/../../bin/console queue:run ' . $routine->getId() . ' ' . QueueRunCommand::TYPE_ROUTINE, QueueScheduleCommand::BIN_PHP)
                  ->onlyOne()
                  ->output(__DIR__ . '/../../var/log/routine.log')
        ;
        $scheduler->run();
    }
}