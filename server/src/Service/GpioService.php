<?php

namespace App\Service;

use PiPHP\GPIO\GPIO;
use PiPHP\GPIO\Pin\PinInterface;

/**
 * Class GpioService
 *
 * @package App\Service
 */
class GpioService
{
    /**
     * @var GPIO|null
     */
    protected ?GPIO $gpio = null;

    /**
     * @return GPIO
     */
    protected function getGpio(): GPIO
    {
        if (!is_null($this->gpio)) {
            return $this->gpio;
        }

        $this->gpio = new GPIO();

        return $this->gpio;
    }

    /**
     * @param int $pinId
     */
    public function enable(int $pinId): void
    {
        $this->changeStatus($pinId, PinInterface::VALUE_HIGH);
    }

    /**
     * @param int $pinId
     */
    public function disable(int $pinId): void
    {
        $this->changeStatus($pinId, PinInterface::VALUE_LOW);
    }

    /**
     * @param int $pinId
     * @param int $value
     */
    protected function changeStatus(int $pinId, int $value): void
    {
        try {
            $pin = $this->getGpio()->getOutputPin($pinId);
            $pin->setValue($value);
        } catch (\Exception $exception) {
            file_put_contents(__DIR__ . '/../../var/log/rpi.log', $exception->getMessage(), FILE_APPEND);
        }
    }
}