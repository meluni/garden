<?php

namespace App\Service\Builder;

use App\Entity\Routine;
use App\Entity\RoutineAction;
use App\Entity\SprinklerGroup;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class RoutineActionBuilder
 *
 * @package App\Service\Builder
 */
class RoutineActionBuilder extends AbstractBuilder
{
    public function fromRequest(Request $request)
    {
        $content = $this->getContent($request);

        /** @var RoutineAction $routineAction */
        $routineAction = $this->getEntity($request, RoutineAction::class);
        $routineAction->setDuration($content->duration);
        $routineAction->setPosition($content->position);
        $routineAction->setRoutine($this->entityManager->getRepository(Routine::class)->find($content->routine->id));
        $routineAction->setSprinklerGroup($this->entityManager->getRepository(SprinklerGroup::class)->find($content->sprinklerGroup->id));

        return $routineAction;
    }
}