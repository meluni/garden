<?php

namespace App\Service\Builder;

use App\Entity\Routine;
use App\Entity\RoutineAction;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class RoutineBuilder
 *
 * @package App\Service\Builder
 */
class RoutineBuilder extends AbstractBuilder
{
    /**
     * @var RoutineActionBuilder
     */
    protected $routineActionBuilder;

    /**
     * RoutineBuilder constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param RoutineActionBuilder   $routineActionBuilder
     */
    public function __construct(EntityManagerInterface $entityManager, RoutineActionBuilder $routineActionBuilder)
    {
        parent::__construct($entityManager);
        $this->routineActionBuilder = $routineActionBuilder;
    }

    public function fromRequest(Request $request)
    {
        /** @var Routine $routine */
        $routine = $this->getEntity($request, Routine::class);
        $routine->setName($this->getContent($request)->name);

        return $routine;
    }
}