<?php

namespace App\Service\Builder;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AbstractBuilder
 *
 * @package App\Service\Builder
 */
abstract class AbstractBuilder implements RequestBuilderInterface
{
    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var object
     */
    protected $content;

    /**
     * AbstractBuilder constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param Request $request
     * @param string  $class
     *
     * @return object|null
     */
    protected function getEntity(Request $request, string $class)
    {
        $content = $this->getContent($request);
        if (!isset($content->id)) {
            return new $class();
        }

        return $this->entityManager->getRepository($class)->find($content->id);
    }

    /**
     * @param Request $request
     *
     * @return object
     */
    protected function getContent(Request $request)
    {
        if ($this->content) {
            return $this->content;
        }

        $content = $request->getContent();
        $this->content = json_decode($content);

        return $this->content;
    }
}