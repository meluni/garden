<?php
namespace App\Service\Builder;

use Symfony\Component\HttpFoundation\Request;

/**
 * Interface RequestBuilderInterface
 *
 * @package Aoo\Service\Builder
 */
interface RequestBuilderInterface
{
    /**
     * @param Request $request
     *
     * @return mixed
     */
    public function fromRequest(Request $request);
}