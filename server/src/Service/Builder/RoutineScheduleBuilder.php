<?php

namespace App\Service\Builder;

use App\Entity\Routine;
use App\Entity\RoutineSchedule;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class RoutineScheduleBuilder
 *
 * @package App\Service\Builder
 */
class RoutineScheduleBuilder extends AbstractBuilder
{
    /**
     * @param Request $request
     *
     * @return RoutineSchedule|mixed
     */
    public function fromRequest(Request $request)
    {
        $content = $this->getContent($request);

        /** @var RoutineSchedule $routineSchedule */
        $routineSchedule = $this->getEntity($request, RoutineSchedule::class);
        $routineSchedule->setRoutine($this->entityManager->getRepository(Routine::class)->find($content->routine->id));
        $routineSchedule->setMinute($content->minute);
        $routineSchedule->setHour($content->hour);
        $routineSchedule->setDay($content->day);
        $routineSchedule->setMonth($content->month);
        $routineSchedule->setWeekDay($content->weekDay);

        return $routineSchedule;
    }
}