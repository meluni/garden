<?php

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;

class AbstractService
{
    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * SprinklerGroupService constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }
}