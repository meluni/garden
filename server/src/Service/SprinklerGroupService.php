<?php

namespace App\Service;

use App\Entity\SprinklerGroup;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class SprinklerGroupService
 *
 * @package App\Service
 */
class SprinklerGroupService extends AbstractService
{
    /**
     * @var SprinklerService
     */
    protected $sprinklerService;

    protected GpioService $gpioService;

    /**
     * SprinklerGroupService constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param SprinklerService       $sprinklerService
     */
    public function __construct(EntityManagerInterface $entityManager, SprinklerService $sprinklerService, GpioService $gpioService)
    {
        parent::__construct($entityManager);
        $this->sprinklerService = $sprinklerService;
        $this->gpioService = $gpioService;
    }

    /**
     * @param SprinklerGroup $group
     */
    public function activate(SprinklerGroup $group)
    {
        $this->gpioService->enable($group->getPin());;

        foreach ($group->getSprinklers() as $sprinkler) {
            $this->sprinklerService->activate($sprinkler);
        }
    }

    /**
     * @param SprinklerGroup $group
     */
    public function deactivate(SprinklerGroup $group)
    {
        $this->gpioService->disable($group->getPin());;

        foreach ($group->getSprinklers() as $sprinkler) {
            $this->sprinklerService->deactivate($sprinkler);
        }
    }
}