<?php

namespace App\Service;

use App\Entity\History;
use App\Entity\Sprinkler;

/**
 * Class SprinklerService
 *
 * @package App\Service
 */
class SprinklerService extends AbstractService
{
    /**
     * @param Sprinkler $sprinkler
     */
    public function activate(Sprinkler $sprinkler): void
    {
        $this->updateSprinkler($sprinkler, Sprinkler::STATUS_ENABLED);
    }

    /**
     * @param Sprinkler $sprinkler
     */
    public function deactivate(Sprinkler $sprinkler): void
    {
        $this->updateSprinkler($sprinkler, Sprinkler::STATUS_DISABLED);
    }

    /**
     * @param Sprinkler $sprinkler
     * @param int       $status
     */
    protected function updateSprinkler(Sprinkler $sprinkler, int $status): void
    {
        $sprinkler->setStatus($status);
        $this->entityManager->persist($sprinkler);
        $this->entityManager->flush();;
    }
}