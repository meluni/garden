<?php

namespace App\Repository;

use App\Entity\SprinklerGroupQueue;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SprinklerGroupQueue|null find($id, $lockMode = null, $lockVersion = null)
 * @method SprinklerGroupQueue|null findOneBy(array $criteria, array $orderBy = null)
 * @method SprinklerGroupQueue[]    findAll()
 * @method SprinklerGroupQueue[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SprinklerGroupQueueRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SprinklerGroupQueue::class);
    }

    // /**
    //  * @return SprinklerGroupQueue[] Returns an array of SprinklerGroupQueue objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SprinklerGroupQueue
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
