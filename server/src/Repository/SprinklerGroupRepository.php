<?php

namespace App\Repository;

use App\Entity\SprinklerGroup;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SprinklerGroup|null find($id, $lockMode = null, $lockVersion = null)
 * @method SprinklerGroup|null findOneBy(array $criteria, array $orderBy = null)
 * @method SprinklerGroup[]    findAll()
 * @method SprinklerGroup[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SprinklerGroupRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SprinklerGroup::class);
    }

    // /**
    //  * @return SprinklerGroup[] Returns an array of SprinklerGroup objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SprinklerGroup
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
