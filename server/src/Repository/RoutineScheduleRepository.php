<?php

namespace App\Repository;

use App\Entity\RoutineSchedule;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RoutineSchedule|null find($id, $lockMode = null, $lockVersion = null)
 * @method RoutineSchedule|null findOneBy(array $criteria, array $orderBy = null)
 * @method RoutineSchedule[]    findAll()
 * @method RoutineSchedule[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RoutineScheduleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RoutineSchedule::class);
    }

    // /**
    //  * @return RoutineSchedule[] Returns an array of RoutineSchedule objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RoutineSchedule
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
