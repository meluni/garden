<?php

namespace App\Command;

use App\Entity\History;
use App\Entity\Routine;
use App\Entity\RoutineSchedule;
use App\Entity\SprinklerGroup;
use App\Service\SprinklerGroupService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class QueueRunCommand
 *
 * @package App\Command
 */
class QueueRunCommand extends Command
{
    public const TYPE_ROUTINE          = 'routine';
    public const TYPE_ROUTINE_SCHEDULE = 'routineSchedule';

    protected static $defaultName = 'queue:run';

    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var SprinklerGroupService
     */
    protected $sprinklerGroupService;

    /**
     * QueueRunCommand constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param SprinklerGroupService  $sprinklerGroupService
     * @param string|null            $name
     */
    public function __construct(EntityManagerInterface $entityManager, SprinklerGroupService $sprinklerGroupService, string $name = null)
    {
        $this->entityManager = $entityManager;
        $this->sprinklerGroupService = $sprinklerGroupService;
        parent::__construct($name);
    }

    protected function configure()
    {
        $this
            ->setDescription('Run a routine schedule by id')
            ->addArgument('id', InputArgument::REQUIRED)
            ->addArgument('type', InputArgument::REQUIRED)
        ;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $id = $input->getArgument('id');
        $type = $input->getArgument('type');

        switch ($type) {
            case self::TYPE_ROUTINE_SCHEDULE:
                /** @var RoutineSchedule $routineSchedule */
                $routineSchedule = $this->entityManager->getRepository(RoutineSchedule::class)->find($id);

                if (!$routineSchedule) {
                    $output->write('Unable to find routine schedule ' . $id);
                    return 404;
                }

                $routine = $routineSchedule->getRoutine();
                break;
            case self::TYPE_ROUTINE;
                $routineSchedule = null;
                $routine = $this->entityManager->getRepository(Routine::class)->find($id);
                if (!$routine) {
                    $output->write('Unable to find routine ' . $id);
                    return 404;
                }

                break;
            default:
                $output->write('Please specify a type');
                return 400;
        }

        $actions = $routine->getSortedRoutineActions();
        foreach ($actions as $action) {
            $group = $action->getSprinklerGroup();
            $this->sprinklerGroupService->activate($group);

            $this->addHistory($routine, $group, History::ACTION_START, $routineSchedule);

            sleep($action->getDuration() * 60);
            $this->sprinklerGroupService->deactivate($group);

            sleep(5); // Give the system some time to breath
            $this->addHistory($routine, $group, History::ACTION_STOP, $routineSchedule);
        }

        return 0;
    }

    /**
     * @param Routine              $routine
     * @param SprinklerGroup       $group
     * @param int                  $action
     * @param RoutineSchedule|null $routineSchedule
     *
     * @return History
     * @throws \Exception
     */
    private function addHistory(Routine $routine, SprinklerGroup $group, int $action, RoutineSchedule $routineSchedule = null): History
    {
        $history = new History();

        $history->setRoutine($routine);
        $history->setSprinklerGroup($group);
        $history->setAction(History::ACTION_START);
        $history->setCreatedAt(new \DateTime());
        $history->setRoutineSchedule($routineSchedule);
        $history->setAction($action);
        $this->entityManager->persist($history);
        $this->entityManager->flush();

        return $history;
    }
}