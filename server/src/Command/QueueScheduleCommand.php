<?php

namespace App\Command;

use App\Entity\RoutineSchedule;
use Doctrine\ORM\EntityManagerInterface;
use GO\Scheduler;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class QueueScheduleCommand
 */
class QueueScheduleCommand extends Command
{
    const BIN_PHP = '/usr/bin/php';

    /**
     * @var string
     */
    protected static $defaultName = 'queue:schedule';

    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * QueueScheduleCommand constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param string|null            $name
     */
    public function __construct(EntityManagerInterface $entityManager, string $name = null)
    {
        parent::__construct($name);
        $this->entityManager = $entityManager;
    }

    protected function configure()
    {
        $this
            ->setDescription('Schedules routines');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $scheduler = new Scheduler();

        $routineSchedules = $this->entityManager->getRepository(RoutineSchedule::class)->findAll();

        /** @var RoutineSchedule $routineSchedule */
        foreach ($routineSchedules as $routineSchedule) {
            $scheduler->php(__DIR__ . '/../../bin/console queue:run ' . $routineSchedule->getId() . ' ' . QueueRunCommand::TYPE_ROUTINE_SCHEDULE, self::BIN_PHP, [

            ], sprintf('routineSchedule_%d', $routineSchedule->getId()))
                      ->onlyOne()
                      ->at(sprintf('%s %s %s %s %s',
                          $routineSchedule->getMinute(),
                          $routineSchedule->getHour(),
                          $routineSchedule->getDay(),
                          $routineSchedule->getMonth(),
                          $routineSchedule->getWeekDay()
                      ))
                      ->output(__DIR__ . '/../../var/log/schedule.log')
            ;
        }

        $scheduler->run();

        return 0;
    }
}