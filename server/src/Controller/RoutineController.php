<?php

namespace App\Controller;

use App\Entity\Routine;
use App\Service\Builder\RoutineBuilder;
use App\Service\RoutineService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class RoutineController
 *
 * @package App\Controller
 */
class RoutineController extends AbstractController
{
    /**
     * @var RoutineService
     */
    protected $routineService;

    /**
     * @var RoutineBuilder
     */
    protected $routineBuilder;

    /**
     * RoutineController constructor.
     *
     * @param RoutineService $routineService
     * @param RoutineBuilder $routineBuilder
     */
    public function __construct(RoutineService $routineService, RoutineBuilder $routineBuilder)
    {
        $this->routineService = $routineService;
        $this->routineBuilder = $routineBuilder;
    }

    /**
     * @Route("/api/routine", methods={"GET"})
     *
     * @return JsonResponse
     */
    public function list(): JsonResponse
    {
        /** @var Routine[] $routines */
        $routines = $this->getDoctrine()->getRepository(Routine::class)->findAll();
        $data = [];
        foreach ($routines as $routine) {
            $data[] = $routine->toArray();
        }

        return new JsonResponse($data);
    }

    /**
     * @param int $id
     * @Route("/api/routine/{id}", methods={"GET"})
     *
     * @return JsonResponse
     */
    public function getOne(int $id)
    {
        return new JsonResponse($this->getById($id)->toArray());
    }

    /**
     * @param int $id
     * @Route("/api/routine/activate/{id}", methods={"POST"})
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function activate(int $id): JsonResponse
    {
        $routine = $this->getById($id);
        $this->routineService->activate($routine);

        return new JsonResponse([]);
    }

    /**
     * @param int $id
     * @Route("/api/routine/delete/{id}", methods={"POST"})
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function delete(int $id): JsonResponse
    {
        $routine = $this->getById($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($routine);
        $em->flush();

        return new JsonResponse([]);
    }

    /**
     * @param Request $request
     * @Route("/api/routine", methods={"POST"})
     *
     * @return JsonResponse
     */
    public function create(Request $request): JsonResponse
    {
        $routine = $this->routineBuilder->fromRequest($request);
        $em = $this->getDoctrine()->getManager();
        $em->persist($routine);
        $em->flush();;

        return new JsonResponse(['id' => $routine->getId()], 201);
    }

    /**
     * @param int $id
     *
     * @return Routine
     */
    protected function getById(int $id): Routine
    {
        /** @var Routine $routine */
        $routine = $this->getDoctrine()->getRepository(Routine::class)->find($id);
        if (!$routine) {
            throw new NotFoundHttpException();
        }

        return $routine;
    }
}