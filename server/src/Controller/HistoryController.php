<?php

namespace App\Controller;

use App\Entity\History;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class HistoryController
 *
 * @package App\Controller
 */
class HistoryController extends AbstractController
{
    /**
     * @Route("/api/history", methods={"GET"})
     *
     * @return JsonResponse
     */
    public function list(): JsonResponse
    {
        /** @var History[] $histories */
        $histories = $this->getDoctrine()->getRepository(History::class)->findAll();
        $data = [];
        foreach ($histories as $history) {
            $data[] = $history->toArray();
        }

        return new JsonResponse($data);
    }
}