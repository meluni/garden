<?php

namespace App\Controller;

use App\Entity\RoutineAction;
use App\Service\Builder\RoutineActionBuilder;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class RoutineActionController
 *
 * @package App\Controller
 */
class RoutineActionController extends AbstractController
{
    /**
     * @var RoutineActionBuilder
     */
    protected $routineActionBuilder;

    /**
     * RoutineActionController constructor.
     *
     * @param RoutineActionBuilder $routineActionBuilder
     */
    public function __construct(RoutineActionBuilder $routineActionBuilder)
    {
        $this->routineActionBuilder = $routineActionBuilder;
    }

    /**
     * @Route("/api/routine_action", methods={"GET"})
     *
     * @return JsonResponse
     */
    public function list(): JsonResponse
    {
        /** @var RoutineAction[] $routineActions */
        $routineActions = $this->getDoctrine()->getRepository(RoutineAction::class)->findAll();
        $data = [];
        foreach ($routineActions as $routineAction) {
            $data[] = $routineAction->toArray();
        }

        return new JsonResponse($data);
    }

    /**
     * @param int $id
     * @Route("/api/routine_action/delete/{id}", methods={"POST"})
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function delete(int $id): JsonResponse
    {
        $routine = $this->getById($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($routine);
        $em->flush();

        return new JsonResponse([]);
    }

    /**
     * @param Request $request
     * @Route("/api/routine_action", methods={"POST"})
     *
     * @return JsonResponse
     */
    public function create(Request $request): JsonResponse
    {
        $routineAction = $this->routineActionBuilder->fromRequest($request);
        $em = $this->getDoctrine()->getManager();
        $em->persist($routineAction);
        $em->flush();;

        return new JsonResponse(['id' => $routineAction->getId()], 201);
    }

    /**
     * @param int $id
     *
     * @return RoutineAction
     */
    protected function getById(int $id): RoutineAction
    {
        /** @var RoutineAction $routine */
        $routine = $this->getDoctrine()->getRepository(RoutineAction::class)->find($id);
        if (!$routine) {
            throw new NotFoundHttpException();
        }

        return $routine;
    }
}