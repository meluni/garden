<?php

namespace App\Controller;

use App\Entity\Sprinkler;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class SprinklerController
 *
 * @package App\Controller
 */
class SprinklerController extends AbstractController
{
    /**
     * @Route("/api/sprinkler", methods={"GET"})
     *
     * @return JsonResponse
     */
    public function list(): JsonResponse
    {
        /** @var Sprinkler[] $sprinklers */
        $sprinklers = $this->getDoctrine()->getRepository(Sprinkler::class)->findAll();
        $data = [];
        foreach ($sprinklers as $sprinkler) {
            $data[] = $sprinkler->toArray();
        }

        return new JsonResponse($data);
    }

    /**
     * @Route("/api/sprinkler/{id}", methods={"GET"})
     * @param int $id
     *
     * @return JsonResponse
     */
    public function getOne(int $id): JsonResponse
    {
        /** @var Sprinkler $sprinkler */
        $sprinkler = $this->getDoctrine()->getRepository(Sprinkler::class)->find($id);
        if (!$sprinkler) {
            throw new NotFoundHttpException();
        }

        return new JsonResponse($sprinkler->toArray());
    }
}