<?php

namespace App\Controller;

use App\Entity\SprinklerGroup;
use App\Service\SprinklerGroupService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class SprinklerGroupController
 *
 * @package App\Controller
 */
class SprinklerGroupController extends AbstractController
{
    /**
     * @var SprinklerGroupService
     */
    protected $sprinklerGroupService;

    /**
     * SprinklerGroupController constructor.
     *
     * @param SprinklerGroupService $sprinklerGroupService
     */
    public function __construct(SprinklerGroupService $sprinklerGroupService)
    {
        $this->sprinklerGroupService = $sprinklerGroupService;
    }

    /**
     * @Route("/api/sprinkler_group", methods={"GET"})
     *
     * @return JsonResponse
     */
    public function list(): JsonResponse
    {
        /** @var SprinklerGroup[] $groups */
        $groups = $this->getDoctrine()->getRepository(SprinklerGroup::class)->findAll();
        $data = [];
        foreach ($groups as $group) {
            $data[] = $group->toArray();
        }

        return new JsonResponse($data);
    }

    /**
     * @Route("/api/sprinkler_group/{id}", methods={"GET"})
     * @param int $id
     *
     * @return JsonResponse
     */
    public function getOne(int $id): JsonResponse
    {
        return new JsonResponse($this->getGroupById($id)->toArray());
    }

    /**
     * @Route("/api/sprinkler_group/activate/{id}", methods={"POST"})
     * @param int $id
     *
     * @return JsonResponse
     */
    public function activate(int $id): JsonResponse
    {
        $this->sprinklerGroupService->activate($this->getGroupById($id));

        return new JsonResponse([]);
    }

    /**
     * @Route("/api/sprinkler_group/deactivate/{id}", methods={"POST"})
     * @param int $id
     *
     * @return JsonResponse
     */
    public function deactivate(int $id): JsonResponse
    {
        $this->sprinklerGroupService->deactivate($this->getGroupById($id));
        return new JsonResponse([]);
    }

    /**
     * @param int $id
     *
     * @return SprinklerGroup
     */
    protected function getGroupById(int $id): SprinklerGroup
    {
        /** @var SprinklerGroup $group */
        $group = $this->getDoctrine()->getRepository(SprinklerGroup::class)->find($id);
        if (!$id) {
            throw new NotFoundHttpException('Unable to find sprinklerGroup with id ' . $id);
        }

        return $group;
    }
}