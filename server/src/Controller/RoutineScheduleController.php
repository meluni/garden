<?php

namespace App\Controller;

use App\Entity\RoutineSchedule;
use App\Service\Builder\RoutineScheduleBuilder;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class RoutineScheduleController
 *
 * @package App\Controller
 */
class RoutineScheduleController extends AbstractController
{
    /**
     * @var RoutineScheduleBuilder
     */
    protected $routineScheduleBuilder;

    /**
     * RoutineScheduleController constructor.
     *
     * @param RoutineScheduleBuilder $routineScheduleBuilder
     */
    public function __construct(RoutineScheduleBuilder $routineScheduleBuilder)
    {
        $this->routineScheduleBuilder = $routineScheduleBuilder;
    }

    /**
     * @Route("/api/routine_schedule", methods={"GET"})
     *
     * @return JsonResponse
     */
    public function list(): JsonResponse
    {
        /** @var RoutineSchedule[] $routineSchedules */
        $routineSchedules = $this->getDoctrine()->getRepository(RoutineSchedule::class)->findAll();
        $data = [];
        foreach ($routineSchedules as $routineSchedule) {
            $data[] = $routineSchedule->toArray();
        }

        return new JsonResponse($data);
    }

    /**
     * @param int $id
     * @Route("/api/routine_schedule/{id}", methods={"GET"})
     *
     * @return JsonResponse
     */
    public function getOne(int $id): JsonResponse
    {
        return new JsonResponse($this->getById($id)->toArray());
    }

    /**
     * @param int $id
     * @Route("/api/routine_schedule/delete/{id}", methods={"POST"})
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function delete(int $id): JsonResponse
    {
        $routine = $this->getById($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($routine);
        $em->flush();

        return new JsonResponse([]);
    }

    /**
     * @param Request $request
     * @Route("/api/routine_schedule", methods={"POST"})
     *
     * @return JsonResponse
     */
    public function create(Request $request): JsonResponse
    {
        $routine = $this->routineScheduleBuilder->fromRequest($request);
        $em = $this->getDoctrine()->getManager();
        $em->persist($routine);
        $em->flush();;

        return new JsonResponse(['id' => $routine->getId()], 201);
    }

    /**
     * @param int $id
     *
     * @return RoutineSchedule
     */
    protected function getById(int $id): RoutineSchedule
    {
        /** @var RoutineSchedule $routine */
        $routine = $this->getDoctrine()->getRepository(RoutineSchedule::class)->find($id);
        if (!$routine) {
            throw new NotFoundHttpException();
        }

        return $routine;
    }
}