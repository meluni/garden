<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200425125449 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE sprinkler (id INT AUTO_INCREMENT NOT NULL, sprinkler_group_id INT NOT NULL, name VARCHAR(255) NOT NULL, position_x SMALLINT NOT NULL, position_y SMALLINT NOT NULL, INDEX IDX_FA12E1262A4DF569 (sprinkler_group_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sprinkler_group (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE history (id INT AUTO_INCREMENT NOT NULL, routine_id INT DEFAULT NULL, routine_schedule_id INT DEFAULT NULL, sprinkler_group_id INT NOT NULL, action SMALLINT NOT NULL, created_at DATETIME NOT NULL, INDEX IDX_27BA704BF27A94C7 (routine_id), INDEX IDX_27BA704BBD3E88DD (routine_schedule_id), INDEX IDX_27BA704B2A4DF569 (sprinkler_group_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE routine (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE routine_action (id INT AUTO_INCREMENT NOT NULL, routine_id INT NOT NULL, sprinkler_group_id INT NOT NULL, duration SMALLINT NOT NULL, position SMALLINT NOT NULL, INDEX IDX_3E0B9EDF27A94C7 (routine_id), INDEX IDX_3E0B9ED2A4DF569 (sprinkler_group_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE routine_schedule (id INT AUTO_INCREMENT NOT NULL, routine_id INT NOT NULL, day VARCHAR(10) NOT NULL, minute VARCHAR(10) NOT NULL, hour VARCHAR(10) NOT NULL, week_day VARCHAR(10) NOT NULL, month VARCHAR(10) NOT NULL, INDEX IDX_A42CF4CDF27A94C7 (routine_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE sprinkler ADD CONSTRAINT FK_FA12E1262A4DF569 FOREIGN KEY (sprinkler_group_id) REFERENCES sprinkler_group (id)');
        $this->addSql('ALTER TABLE history ADD CONSTRAINT FK_27BA704BF27A94C7 FOREIGN KEY (routine_id) REFERENCES routine (id)');
        $this->addSql('ALTER TABLE history ADD CONSTRAINT FK_27BA704BBD3E88DD FOREIGN KEY (routine_schedule_id) REFERENCES routine_schedule (id)');
        $this->addSql('ALTER TABLE history ADD CONSTRAINT FK_27BA704B2A4DF569 FOREIGN KEY (sprinkler_group_id) REFERENCES sprinkler_group (id)');
        $this->addSql('ALTER TABLE routine_action ADD CONSTRAINT FK_3E0B9EDF27A94C7 FOREIGN KEY (routine_id) REFERENCES routine (id)');
        $this->addSql('ALTER TABLE routine_action ADD CONSTRAINT FK_3E0B9ED2A4DF569 FOREIGN KEY (sprinkler_group_id) REFERENCES sprinkler_group (id)');
        $this->addSql('ALTER TABLE routine_schedule ADD CONSTRAINT FK_A42CF4CDF27A94C7 FOREIGN KEY (routine_id) REFERENCES routine (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE sprinkler DROP FOREIGN KEY FK_FA12E1262A4DF569');
        $this->addSql('ALTER TABLE history DROP FOREIGN KEY FK_27BA704B2A4DF569');
        $this->addSql('ALTER TABLE routine_action DROP FOREIGN KEY FK_3E0B9ED2A4DF569');
        $this->addSql('ALTER TABLE history DROP FOREIGN KEY FK_27BA704BF27A94C7');
        $this->addSql('ALTER TABLE routine_action DROP FOREIGN KEY FK_3E0B9EDF27A94C7');
        $this->addSql('ALTER TABLE routine_schedule DROP FOREIGN KEY FK_A42CF4CDF27A94C7');
        $this->addSql('ALTER TABLE history DROP FOREIGN KEY FK_27BA704BBD3E88DD');
        $this->addSql('DROP TABLE sprinkler');
        $this->addSql('DROP TABLE sprinkler_group');
        $this->addSql('DROP TABLE history');
        $this->addSql('DROP TABLE routine');
        $this->addSql('DROP TABLE routine_action');
        $this->addSql('DROP TABLE routine_schedule');
    }
}
