<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200505135241 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE sprinkler_group ADD pin SMALLINT NOT NULL');
        $this->addSql('UPDATE sprinkler_group SET pin = :pin WHERE id = :id', [
            'id' => 1,
            'pin' => 36
        ]);

        $this->addSql('UPDATE sprinkler_group SET pin = :pin WHERE id = :id', [
            'id' => 2,
            'pin' => 29
        ]);

        $this->addSql('UPDATE sprinkler_group SET pin = :pin WHERE id = :id', [
            'id' => 3,
            'pin' => 33
        ]);

        $this->addSql('UPDATE sprinkler_group SET pin = :pin WHERE id = :id', [
            'id' => 4,
            'pin' => 31
        ]);
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE sprinkler_group DROP pin');
    }
}
