<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200430214027 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('UPDATE sprinkler SET position_x = 350, position_y = 320 WHERE id = 1');
        $this->addSql('UPDATE sprinkler SET position_x = 130, position_y = 380 WHERE id = 2');
        $this->addSql('UPDATE sprinkler SET position_x = 275, position_y = 125 WHERE id = 3');
        $this->addSql('UPDATE sprinkler SET position_x = 30, position_y = 330, name = \'border\' WHERE id = 4');
        $this->addSql('UPDATE sprinkler SET position_x = 30, position_y = 125 WHERE id = 5');
    }

    public function down(Schema $schema) : void
    {

    }
}