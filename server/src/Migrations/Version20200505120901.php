<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200505120901 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE sprinkler ADD area VARCHAR(5000) NOT NULL');
        $this->addSql('UPDATE sprinkler SET area = :area WHERE id = :id', [
            'id' => 1,
            'area' => '[{"type":"moveTo","values":{"x":400,"y":340}},{"type":"lineTo","values":{"x":100,"y":423}},{"type":"quadraticCurveTo","values":{"x1":40,"y1":100,"x2":400,"y2":100}}]',
        ]);

        $this->addSql('UPDATE sprinkler SET area = :area WHERE id = :id', [
            'id' => 2,
            'area' => '[{"type":"moveTo","values":{"x":100,"y":423}},{"type":"lineTo","values":{"x":400,"y":340}},{"type":"quadraticCurveTo","values":{"x1":400,"y1":100,"x2":100,"y2":100}}]',
        ]);

        $this->addSql('UPDATE sprinkler SET area = :area WHERE id = :id', [
            'id' => 3,
            'area' => '[{"type":"moveTo","values":{"x":150,"y":0}},{"type":"lineTo","values":{"x":400,"y":0}},{"type":"lineTo","values":{"x":400,"y":230}},{"type":"lineTo","values":{"x":260,"y":150}}]',
        ]);

        $this->addSql('UPDATE sprinkler SET area = :area WHERE id = :id', [
            'id' => 4,
            'area' => '[{"type":"moveTo","values":{"x":0,"y":452}},{"type":"lineTo","values":{"x":0,"y":250}},{"type":"lineTo","values":{"x":100,"y":250}},{"type":"lineTo","values":{"x":100,"y":425}}]',
        ]);

        $this->addSql('UPDATE sprinkler SET area = :area WHERE id = :id', [
            'id' => 5,
            'area' => '[{"type":"moveTo","values":{"x":0,"y":0}},{"type":"lineTo","values":{"x":200,"y":0}},{"type":"quadraticCurveTo","values":{"x1":400,"y1":200,"x2":0,"y2":300}}]',
        ]);
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE sprinkler DROP area');
    }
}
