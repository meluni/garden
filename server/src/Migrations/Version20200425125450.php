<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200425125450 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('INSERT INTO sprinkler_group (id, name) VALUES (1, \'Groep 1\'), (2, \'Groep 2\'), (3, \'Groep 3\'), (4, \'Groep 4\')');
        $this->addSql('INSERT INTO sprinkler (sprinkler_group_id, name, position_x, position_y) VALUES (1, \'Rechts voor\', 0,0), (2, \'Links voor\', 0,0), (3, \'Midden\', 0,0), (4, \'Links voor\', 0,0), (4, \'Links achter\', 0,0)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DELETE FROM sprinkler');
        $this->addSql('DELETE FROM sprinkler_group');
    }
}
