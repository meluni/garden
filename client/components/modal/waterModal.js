import Axios from "axios";

class WaterModal extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            sprinklerGroup: null,
            sprinklerGroupId: props.sprinklerGroup,
            successMessage: null,
        }

        this.hide = this.hide.bind(this);
        this.activate = this.activate.bind(this);
        this.deactivate = this.deactivate.bind(this);
    }

    componentDidMount() {

        if (!this.state.sprinklerGroupId) {
            return;
        }

        const parent = this;

        Axios.get('https://api.tuin.paulcelie.nl/api/sprinkler_group/' + this.state.sprinklerGroupId)
            .then(function (response) {
                parent.setState({
                    sprinklerGroup: response.data,
                })

            });
    }

    componentWillReceiveProps(newProps) {
        if (!newProps.sprinklerGroup) {
            return;
        }
        
        this.setState({sprinklerGroupId: newProps.sprinklerGroup});

        const parent = this;

        Axios.get('https://api.tuin.paulcelie.nl/api/sprinkler_group/' + newProps.sprinklerGroup)
            .then(function (response) {
                parent.setState({
                    sprinklerGroup: response.data,
                })

            });
    }

    activate() {
        const parent = this;

        Axios.post('https://api.tuin.paulcelie.nl/api/sprinkler_group/activate/' + this.state.sprinklerGroup.id)
            .then(function (response) {
                parent.setState({
                    successMessage: 'De sprinklers zijn aangezet',
                })

                Axios.get('https://api.tuin.paulcelie.nl/api/sprinkler_group/' + parent.state.sprinklerGroupId)
                    .then(function (response) {
                        parent.setState({
                            sprinklerGroup: response.data,
                        })

                        parent.hide();
                    })
            });
    }

    deactivate() {
        const parent = this;

        Axios.post('https://api.tuin.paulcelie.nl/api/sprinkler_group/deactivate/' + this.state.sprinklerGroup.id)
            .then(function (response) {
                parent.setState({
                    successMessage: 'De sprinklers zijn uitgezet',
                })

                Axios.get('https://api.tuin.paulcelie.nl/api/sprinkler_group/' + parent.state.sprinklerGroupId)
                    .then(function (response) {
                        parent.setState({
                            sprinklerGroup: response.data,
                        })

                        parent.hide();
                    })
            });
    }

    getContent() {
        if (!this.state.sprinklerGroup) {
            return null;
        }

        const sprinklerGroup = this.state.sprinklerGroup;

        const button = sprinklerGroup.status == 'enabled'
            ? <button style={
                {
                    float: "right",
                    marginRight: "10px",
                }
            } className="btn btn-danger" onClick={this.deactivate}>Zet groep uit</button>
            : <button style={
                {
                    float: "right",
                    marginRight: "10px",
                }
            } className="btn btn-success" onClick={this.activate}>Zet groep aan</button>

        const status = sprinklerGroup.status == 'enabled' ? 'aan' : 'uit';


        const sprinklers = [];
        if (sprinklerGroup.sprinklers) {
            for (var i = 0; i < this.state.sprinklerGroup.sprinklers.length; i++) {
                const sprinkler = this.state.sprinklerGroup.sprinklers[i];
                sprinklers.push(<li>{sprinkler.name}</li>)
            }
        }

        const message = !this.state.successMessage
            ? null
            : (
                <div className="alert alert-success" role="alert">
                    {this.state.successMessage}
                </div>
            )

        const content = (
            <div className="controllerFrom" style={
                {
                    color: '#333333'
                }
            }>
                {button}
                <p>Op dit moment staat deze sprinkergroep <strong>{status}</strong></p>
                <p>De volgende sprinklers behoren tot deze groep</p>
                <ul>
                    {sprinklers}
                </ul>
            </div>
        )

        return content;
    }

    hide() {
        this.props.handler();

        this.setState({
            sprinklerGroup: null,
            sprinklerGroupId: null,
            successMessage: null,
        })
    }

    render() {

        if (!this.state.sprinklerGroupId) {
            return null;
        }

        return (
            <div style={
                {
                    backgroundColor: "white",
                    position: "absolute",
                    top: "0px",
                    height: "100%",
                    width: "100%",
                    padding: "1em",

                }
            }>
            <span style={
                {
                    float: "right",
                    marginRight: "10px",
                    cursor: "pointer",
                    fontSize: "20px",
                }
            } onClick={this.hide}>X</span>
                <h3 className="primary" style={
                    {
                        color: "#1d1d1d"
                    }
                }>{!this.state.sprinklerGroup ? 'Bezig met laden...' : this.state.sprinklerGroup.name}</h3>
                {this.getContent()}
            </div>
        )
    }
}

export default WaterModal;