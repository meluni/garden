import {Circle, Shape} from "react-konva";
import Axios from "axios";

class Water extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            items: [],
            reload: props.reload
        }
    }

    componentDidMount() {
        const parent = this;

        Axios.get('https://api.tuin.paulcelie.nl/api/sprinkler')
            .then(function (response) {
                parent.setState({
                    items: response.data,
                })
            });
    }

    componentWillReceiveProps(newProps) {
        if (this.state.reload === newProps.reload) {
            return;
        }

        const parent = this;

        Axios.get('https://api.tuin.paulcelie.nl/api/sprinkler')
            .then(function (response) {
                parent.setState({
                    items: response.data,
                })
            });
    }

    openModal = e => {
        return this.props.handler(e);
    };

    generateContext(context, data) {
        for (let i=0; i<data.length; i++) {
            const row = data[i];
            const values = row.values;
            switch (row.type) {
                case 'moveTo':
                    context.moveTo(values.x, values.y);
                    break;
                case 'lineTo':
                    context.lineTo(values.x, values.y);
                    break;
                case 'quadraticCurveTo':
                    context.quadraticCurveTo(values.x1, values.y1, values.x2, values.y2);
                    break;
                default:
                    break;
            }
        }
    }

    render() {
        const sprinklers = [];
        for (let i = 0; i < this.state.items.length; i++) {
            const item = this.state.items[i];
            if (item.status !== 'enabled') {
                continue;
            }

            sprinklers.push(
                <Shape
                    sceneFunc={(context, shape) => {
                        context.beginPath();
                        this.generateContext(context, item.area);
                        context.closePath();
                        context.fillStrokeShape(shape);
                    }}
                    fill="#54A4DE"
                    strokeWidth={0}
                    opacity={0.6}
                />
            )

        }

        for (let i = 0; i < this.state.items.length; i++) {
            const item = this.state.items[i];
            const color = item.status == 'enabled' ? '#54A4DE' : '#EEEEEE';
            sprinklers.push(<Circle group={item.sprinklerGroup.id} sprinkler={item.name} x={item.positionX} y={item.positionY} radius={20} fill={color} stroke='black' strokeWidth={1} onMouseDown={this.openModal} onTouchStart={this.openModal} />)
        }

        return sprinklers;
    }
}

export default Water;

