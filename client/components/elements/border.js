import {Shape} from "react-konva";

class Border extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            image: null
        }
    }

    componentDidMount() {
        const parent = this;

        const imageObj = new Image();
        imageObj.src = '/image/background/zand.png';
        imageObj.onload = function () {
            parent.setState({
                image: imageObj
            })
        }
    }

    render() {
        return this.state.image
            ? <Shape
                sceneFunc={(context, shape) => {
                    context.beginPath();
                    context.moveTo(0, 452);
                    context.lineTo(0, 250);
                    context.lineTo(100, 250);
                    context.lineTo(100, 424);
                    context.closePath();
                    context.fillStrokeShape(shape);
                }}
                fillPatternImage={this.state.image}
                strole="#614933"
                strokeWidth={1}
            />
            : null

    }
}

export default Border;