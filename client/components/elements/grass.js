import {Shape} from "react-konva";

class Grass extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            image: null
        }
    }

    componentDidMount() {
        const parent = this;

        const imageObj = new Image();
        imageObj.src = '/image/background/grass.jpg';
        imageObj.onload = function () {
            parent.setState({
                image: imageObj
            })
        }
    }

    render() {
        return this.state.image
            ? <Shape
                sceneFunc={(context, shape) => {
                    context.beginPath();
                    context.moveTo(0, 0);
                    context.lineTo(400, 0);
                    context.lineTo(400, 340);
                    context.lineTo(0, 452);
                    context.closePath();
                    context.fillStrokeShape(shape);
                }}
                fillPatternImage={this.state.image}
                strokeWidth={0}
            />
            : null

    }
}

export default Grass;