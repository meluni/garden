import Axios from "axios";
import RoutineActionList from "../list/routineActionList";
import RoutineScheduleList from "../list/routineScheduleList";

class RoutineScheduleForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            id: null,
            routines: [],
            routine: '',
            minute: '',
            hour: '',
            day: '',
            month: '',
            weekDay: '',
            showForm: false,
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.listHandler = this.listHandler.bind(this);
        this.handleAddForm = this.handleAddForm.bind(this);
    }

    handleChange(event) {
        const name = event.target.name;
        this.setState({[name]: event.target.value});
    }

    handleSubmit(event) {
        const parent = this;

        const data = {
            "id": this.state.id,
            "minute": this.state.minute,
            "hour": this.state.hour,
            "day": this.state.day,
            "month": this.state.month,
            "weekDay": this.state.weekDay,
            "routine": {
                "id": this.state.routine,
            },
        }

        Axios.post('https://api.tuin.paulcelie.nl/api/routine_schedule', data)
            .then(function (response) {
                parent.setState({
                    id: null,
                    routine: '',
                    minute: '',
                    hour: '',
                    day: '',
                    month: '',
                    weekDay: '',
                    showForm: false,
                });
            });

        event.preventDefault();
    }

    handleAddForm(event) {
        this.setState({
            id: null,
            routine: '',
            minute: '',
            hour: '',
            day: '',
            month: '',
            weekDay: '',
            showForm: true,
        });
    }

    listHandler(event) {

        this.setState({
                id: event.target.dataset.id,
                routine: event.target.dataset.routine,
                minute: event.target.dataset.minute,
                hour: event.target.dataset.hour,
                day: event.target.dataset.day,
                month: event.target.dataset.month,
                weekDay: event.target.dataset.weekDay,
                showForm: true,
            }
        );

    }

    componentDidMount() {
        const parent = this;
        Axios.get('https://api.tuin.paulcelie.nl/api/routine')
            .then(function(response) {
                parent.setState({routines: response.data});
            })
    }

    render() {
        const routines = [];
        routines.push(<option value="">Selecteer routine</option>);
        for (var i = 0; i < this.state.routines.length; i++) {
            const item = this.state.routines[i];
            routines.push(<option value={item.id}>{item.name}</option>);
        }

        if (this.state.showForm) {

            return (
                <form onSubmit={this.handleSubmit}>
                    <div className="form-group">
                        <label htmlFor="">Routine</label>
                        <select value={this.state.routine} onChange={this.handleChange} name="routine"
                                className="form-control">
                            {routines}
                        </select>
                        <label htmlFor="">Minuten</label>
                        <input name="minute" className="form-control" value={this.state.minute}
                               onChange={this.handleChange}/>
                        <label htmlFor="">Uren</label>
                        <input name="hour" className="form-control" value={this.state.hour}
                               onChange={this.handleChange}/>
                        <label htmlFor="">Dag</label>
                        <input name="day" className="form-control" value={this.state.day} onChange={this.handleChange}/>
                        <label htmlFor="">Maand</label>
                        <input name="month" className="form-control" value={this.state.month}
                               onChange={this.handleChange}/>
                        <label htmlFor="">Dag van de week</label>
                        <input name="weekDay" className="form-control" value={this.state.weekDay}
                               onChange={this.handleChange}/>
                        <button type="submit" className="btn btn-primary">Opslaan</button>
                    </div>
                </form>
            )
        }

        return (

            <div>
                <button type="button" className="btn btn-primary" onClick={this.handleAddForm}>+</button>
                <RoutineScheduleList handler={this.listHandler}/>
            </div>
        )
    }
}

export default RoutineScheduleForm;
