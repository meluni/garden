import Axios from "axios";
import RoutineActionList from "../list/routineActionList";

class RoutineActionForm extends React.Component
{
    constructor(props) {
        super(props);
        this.state = {
            value: '',
            group: '',
            duration: '',
            position: '',
            routine: '',
            groups: [],
            routines: [],
            showForm: false,
            id: null,
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.listHandler = this.listHandler.bind(this);
        this.handleAddForm = this.handleAddForm.bind(this);
    }

    handleChange(event) {
        const name = event.target.name;
        this.setState({[name]: event.target.value});
    }


    handleSubmit(event) {
        const parent = this;

        const data = {
            "id": this.state.id,
            "duration": this.state.duration,
            "position": this.state.position,
            "routine": {
                "id": this.state.routine,
            },
            "sprinklerGroup": {
                "id": this.state.group
            }
        }

        Axios.post('https://api.tuin.paulcelie.nl/api/routine_action', data)
            .then(function (response) {
                parent.setState({
                    id: null,
                    value: '',
                    group: '',
                    duration: '',
                    position: '',
                    routine: '',
                    showForm: false,
                });
            });

        event.preventDefault();
    }

    handleAddForm(event) {
        this.setState({
            id: null,
            value: '',
            group: '',
            duration: '',
            position: '',
            routine: '',
            showForm: true,
        });
    }

    listHandler(event) {

        this.setState({
                id: event.target.dataset.id,
                group: event.target.dataset.group,
                routine: event.target.dataset.routine,
                duration: event.target.dataset.duration,
                position: event.target.dataset.position,
                showForm: true,
            }
        );

    }

    componentDidMount() {
        const parent = this;
        Axios.get('https://api.tuin.paulcelie.nl/api/sprinkler_group')
            .then(function(response) {
                parent.setState({groups: response.data});
            });

        Axios.get('https://api.tuin.paulcelie.nl/api/routine')
            .then(function(response) {
                parent.setState({routines: response.data});
            })
    }

    render() {

        if(!this.state.showForm) {
            return (

                <div>
                    <button type="button" className="btn btn-primary" onClick={this.handleAddForm}>+</button>
                    <RoutineActionList handler={this.listHandler}/>
                </div>
            )
        }

        const items = [];
        items.push(<option value="">Selecteer groep</option>);
        for (var i=0; i<this.state.groups.length; i++) {
            const item = this.state.groups[i];
            items.push(<option value={item.id}>{item.name}</option>);
        }

        const routines = [];
        routines.push(<option value="">Selecteer routine</option>);
        for (var i=0; i<this.state.routines.length; i++) {
            const item = this.state.routines[i];
            routines.push(<option value={item.id}>{item.name}</option>);
        }

        const position = [];
        position.push(<option value="">Selecteer positie</option> );
        for (var i=1; i<=10; i++) {
            position.push(<option value={i}>{i}</option> );
        }

        return (
            <form onSubmit={this.handleSubmit}>
            <div className="form-group">
                <label htmlFor="">Routine</label>
                <select value={this.state.routine} onChange={this.handleChange} name="routine" className="form-control">
                    {routines}
                </select>
                <label htmlFor="">Groep</label>
                <select value={this.state.group} onChange={this.handleChange} name="group" className="form-control">
                    {items}
                </select>
                <label htmlFor="duration">Duur in minuten</label>
                <input type="number" id="duration" name="duration" placeholder="Duur (minuten)" className="form-control" value={this.state.duration} onChange={this.handleChange} />
                <label htmlFor="">Positie</label>
                <select value={this.state.position} name="position" onChange={this.handleChange} className="form-control">
                    {position}
                </select>
                <button type="submit" className="btn btn-primary">Opslaan</button>
            </div>
            </form>
        )
    }
}

export default RoutineActionForm;