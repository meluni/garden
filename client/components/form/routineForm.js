import Axios from "axios";
import RoutineList from "../list/routineList";

class RoutineForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: 'Piet',
            id: null,
            showForm: false,
            success: false,
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleAddForm = this.handleAddForm.bind(this);
        this.listHandler = this.listHandler.bind(this);
    }

    handleChange(event) {
        this.setState({name: event.target.value});
    }

    handleSubmit(event) {
        const parent = this;
        Axios.post('https://api.tuin.paulcelie.nl/api/routine', this.state)
            .then(function (response) {
                parent.setState({name: '', id: null, showForm: false, success: true});
            });

        event.preventDefault();
    }

    listHandler(event) {
        this.setState({
                name: event.target.dataset.name,
                id: event.target.dataset.id,
                showForm: true,
            }
        );

    }

    handleAddForm(event) {
        this.setState({name: ''});
        this.setState({showForm: true});
    }

    renderForm() {
        if (this.state.showForm) {
            return (
                <form onSubmit={this.handleSubmit}>
                    <div className="form-group">
                        <label htmlFor="">Routine naam</label>
                        <input type="test" id="name" placeholder="Naam" className="form-control" value={this.state.name}
                               onChange={this.handleChange}/>
                    </div>
                    <button type="submit" class="btn btn-primary">Opslaan</button>
                </form>
            )
        }

        return null;
    }

    render() {

        if (!this.state.showForm) {
            return (
                <div>
                    <button type="button" className="btn btn-primary" onClick={this.handleAddForm}>+</button>
                    <RoutineList handler={this.listHandler}/>
                </div>
            )
        }

        return this.renderForm();
    }
}

export default RoutineForm;