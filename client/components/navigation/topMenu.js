import Link from "next/link";

const TopMenu = () => {
    return <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <Link href="#"><a className="navbar-brand">Beregening</a></Link>
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
        </button>

        <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav mr-auto">
                <li className="nav-item active">
                    <Link href="/"><a className="nav-link">Home</a></Link>
                </li>
                <li className="nav-item">
                    <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Routines</a>
                    <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                        <Link href="/routines"><a className="dropdown-item">Routines</a></Link>
                        <div className="dropdown-divider"></div>
                        <Link href="/routines/actions"><a className="dropdown-item">Acties</a></Link>
                        <Link href="/routines/schedules"><a className="dropdown-item">Planningen</a></Link>
                    </div>

                </li>
                <li className="nav-item">
                    <Link href="/logs"><a className="nav-link">Logs</a></Link>
                </li>
            </ul>
        </div>
    </nav>
}

export default TopMenu