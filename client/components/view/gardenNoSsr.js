import dynamic from "next/dynamic";

const DynamicGarden = dynamic(() => import('./garden'), { ssr: false})

export default DynamicGarden;