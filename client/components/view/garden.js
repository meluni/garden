import {Stage, Circle, Layer, Shape} from "react-konva";
import Grass from "../elements/grass";
import Border from "../elements/border";
import Water from "../elements/water";
import WaterModal from "../modal/waterModal";

class Garden extends React.Component {

    constructor() {
        super();

        this.state = {
            sprinklerGroupId: null,
            reload: 0
        }

        this.handler = this.handler.bind(this)
        this.modalClosed = this.modalClosed.bind(this)
    }

    state = {
        stageWidth: 1000,
    };

    componentDidMount() {
        this.checkSize();
        window.addEventListener("resize", this.checkSize);
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.checkSize);
    }

    handler(event) {
        if (!event) {
            return;
        }

        this.setState({
                sprinklerGroupId: event.target.attrs.group,
            }
        );

    }

    checkSize = () => {
        const width = this.container.offsetWidth;
        this.setState({
            stageWidth: width
        });
    };

    modalClosed() {
        this.setState({
            reload: ++this.state.reload,
            sprinklerGroupId: null,
        });
    }

    render() {

        return (
            <div
                style={{
                    width: "100%",
                    border: "1px solid grey"
                }}
                ref={node => {
                    this.container = node;
                }}
            >
                <Stage width={this.state.stageWidth} height={window.innerHeight}>
                    <Layer>
                        <Grass/>
                        <Border/>
                        <Water handler={this.handler} reload={this.state.reload}/>
                    </Layer>
                </Stage>
                <WaterModal sprinklerGroup={this.state.sprinklerGroupId} handler={this.modalClosed}/>
                <h2>{this.state.sprinklerGroupId}</h2>
            </div>
        );
    }
}

export default Garden;