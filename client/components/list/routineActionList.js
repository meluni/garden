import Axios from "axios";

class RoutineActionList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            items: [],
        }

        this.deleteItem = this.deleteItem.bind(this);
    }

    componentDidMount() {

        const parent = this;

        Axios.get('https://api.tuin.paulcelie.nl/api/routine_action')
            .then(function (response) {
                parent.setState({
                    items: response.data,
                })
            });
    }

    deleteItem(event) {
        const id = event.target.dataset.id;
        const parent = this;
        Axios.post('https://api.tuin.paulcelie.nl/api/routine_action/delete/' + id)
            .then(function (response) {
                Axios.get('https://api.tuin.paulcelie.nl/api/routine_action')
                    .then(function (response) {
                        parent.setState({
                            items: response.data,
                        })
                    });
            });

        event.preventDefault();
    }

    render() {

        const tableBody = [];
        for (var i = 0; i < this.state.items.length; i++) {
            const item = this.state.items[i];
            tableBody.push(
                <tr>
                    <td>{item.routine.name}</td>
                    <td>{item.sprinklerGroup.name}</td>
                    <td>
                        <button className="btn btn-sm btn-outline-info" onClick={this.props.handler} data-routine={item.routine.id} data-group={item.sprinklerGroup.id} data-duration={item.duration} data-position={item.position} data-id={item.id}>Wijzig</button>
                        &nbsp;&nbsp;
                        <button className="btn btn-sm btn-outline-danger" data-id={item.id}
                                onClick={this.deleteItem}>Verwijder
                        </button>
                    </td>
                </tr>
            )
        }

        return (
            <table className="table">
                <thead>
                <tr>
                    <th scope="col">Routine</th>
                    <th scope="col">Groep</th>
                    <th scope="col">Acties</th>
                </tr>
                </thead>
                <tbody>
                {tableBody}
                </tbody>
            </table>
        )
    }
}

export default RoutineActionList;