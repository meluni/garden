import Axios from "axios";

class LogList extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            items: [],
        }
    }

    componentDidMount() {

        const parent = this;

        Axios.get('https://api.tuin.paulcelie.nl/api/history')
            .then(function (response) {
                parent.setState({
                    items: response.data,
                })
            });
    }

    render() {

        const tableBody = [];
        for (var i = 0; i < this.state.items.length; i++) {
            const item = this.state.items[i];
            const routine = item.routine ? item.routine.name : '';
            tableBody.push(
                <tr>
                    <td>{item.createdAt}</td>
                    <td>{routine}</td>
                    <td>{item.sprinklerGroup.name}</td>
                    <td>{item.action}</td>
                </tr>
            )
        }

        return (
            <table className="table">
                <thead>
                <tr>
                    <th scope="col">Datum</th>
                    <th scope="col">Routine</th>
                    <th scope="col">Group</th>
                    <th scope="col">Actie</th>
                </tr>
                </thead>
                <tbody>
                {tableBody}
                </tbody>
            </table>
        )
    }
}

export default LogList;