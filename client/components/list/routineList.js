import Axios from "axios";

class RoutineList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            items: [],
        }

        this.deleteItem = this.deleteItem.bind(this);
    }

    componentDidMount() {

        const parent = this;

        Axios.get('https://api.tuin.paulcelie.nl/api/routine')
            .then(function (response) {
                parent.setState({
                    items: response.data,
                })
            });
    }

    deleteItem(event) {
        const confirmation = confirm('Weet je zeker dat je deze routine wilt verwijderen?');
        if(!confirmation) {
            event.preventDefault();
            return;
        }
        const id = event.target.dataset.id;
        const parent = this;
        Axios.post('https://api.tuin.paulcelie.nl/api/routine/delete/' + id)
            .then(function (response) {
                Axios.get('https://api.tuin.paulcelie.nl/api/routine')
                    .then(function (response) {
                        parent.setState({
                            items: response.data,
                        })
                    });
            });

        event.preventDefault();
    }

    startItem(event) {
        const id = event.target.dataset.id;
        const parent = this;

        Axios.post('https://api.tuin.paulcelie.nl/api/routine/activate/' + id)
            .then(function (response) {

            });
    }

    render() {

        const tableBody = [];
        for (var i = 0; i < this.state.items.length; i++) {
            const item = this.state.items[i];
            tableBody.push(
                <tr>
                    <td>{item.id}</td>
                    <td>{item.name}</td>
                    <td>
                        <button className="btn btn-sm btn-outline-success" data-id={item.id}
                                onClick={this.startItem}>Start
                        </button>
                        &nbsp;&nbsp;
                        <button className="btn btn-sm btn-outline-info" onClick={this.props.handler} data-name={item.name} data-id={item.id}>Wijzig</button>
                        &nbsp;&nbsp;
                        <button className="btn btn-sm btn-outline-danger" data-id={item.id}
                                onClick={this.deleteItem}>Verwijder
                        </button>
                    </td>
                </tr>
            )
        }

        return (
            <table className="table">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Naam</th>
                    <th scope="col">Acties</th>
                </tr>
                </thead>
                <tbody>
                {tableBody}
                </tbody>
            </table>
        )
    }
}

export default RoutineList;