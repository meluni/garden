import Axios from "axios";

class RoutineScheduleList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            items: [],
        }

        this.deleteItem = this.deleteItem.bind(this);
    }

    componentDidMount() {

        const parent = this;

        Axios.get('https://api.tuin.paulcelie.nl/api/routine_schedule')
            .then(function (response) {
                parent.setState({
                    items: response.data,
                })
            });
    }

    deleteItem(event) {
        const id = event.target.dataset.id;
        const parent = this;
        Axios.post('https://api.tuin.paulcelie.nl/api/routine_schedule/delete/' + id)
            .then(function (response) {
                Axios.get('https://api.tuin.paulcelie.nl/api/routine_schedule')
                    .then(function (response) {
                        parent.setState({
                            items: response.data,
                        })
                    });
            });

        event.preventDefault();
    }

    render() {

        const tableBody = [];
        for (var i = 0; i < this.state.items.length; i++) {
            const item = this.state.items[i];
            tableBody.push(
                <tr>
                    <td>{item.routine.name}</td>
                    <td>{item.minute} {item.hour} {item.day} {item.month} {item.weekDay}</td>
                    <td>
                        <button className="btn btn-sm btn-outline-info" onClick={this.props.handler} data-routine={item.routine.id} data-minute={item.minute} data-hour={item.hour} data-day={item.day} data-month={item.month} data-weekDay={item.weekDay} data-id={item.id}>Wijzig</button>
                        &nbsp;&nbsp;
                        <button className="btn btn-sm btn-outline-danger" data-id={item.id}
                                onClick={this.deleteItem}>Verwijder
                        </button>
                    </td>
                </tr>
            )
        }

        return (
            <table className="table">
                <thead>
                <tr>
                    <th scope="col">Routine</th>
                    <th scope="col">Planning</th>
                    <th scope="col">Acties</th>
                </tr>
                </thead>
                <tbody>
                {tableBody}
                </tbody>
            </table>
        )
    }
}

export default RoutineScheduleList;