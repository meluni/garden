import RoutineScheduleForm from "../../components/form/routineScheduleForm";
import Layout from "../../components/layout";

export default function Schedules() {
    return (
        <Layout>
            <div className="container">
                <h1>Routines</h1>
                <RoutineScheduleForm />
            </div>
        </Layout>
    )
}