import RoutineForm from "../../components/form/routineForm";
import Layout from "../../components/layout";
import RoutineActionForm from "../../components/form/routineActionForm";

export default function RoutineActions() {
    return (
        <Layout>
            <div className="container">
                <h1>Routine acties</h1>
                <RoutineActionForm />
            </div>
        </Layout>
    )
}