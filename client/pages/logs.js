import Layout from "../components/layout";
import LogList from "../components/list/logList";

export default function Logs() {
    return (
        <Layout>
            <div className="container">
                <h1>Logs</h1>
                <LogList/>
            </div>
        </Layout>
    )
}