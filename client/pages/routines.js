import Layout from "../components/layout";
import RoutineForm from "../components/form/routineForm";

export default function Routines() {
    return (
        <Layout>
            <div className="container">
                <h1>Routines</h1>
                <RoutineForm />
            </div>
        </Layout>
    )
}