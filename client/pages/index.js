import Layout from "../components/layout";
import DynamicGarden from "../components/view/gardenNoSsr";

export default function Home() {
    return (
        <Layout>
            <div className="container">
                <DynamicGarden />
            </div>
        </Layout>
    )
}
