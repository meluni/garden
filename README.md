### Setup wifi

Change SSID and password
```
sudo vi /etc/netplan/50-cloud-init.yaml
```

Run:
```$xslt
sudo netplan --debug try
sudo netplan --debug apply
sudo reboot
```
